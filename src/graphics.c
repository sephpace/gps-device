/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "graphics.h"


double dot(double v1[2], double v2[2])
{
  return v1[0] * v2[0] + v1[1] * v2[1];
}


/* Parts of this function were organically sourced from this site: https://blackpawn.com/texts/pointinpoly/ */
void render_triangle(uint8_t* buffer, int width, int height, double vertices[3][2], uint8_t color)
{
  // Compute vectors
  double v0[2] = 
  {
    vertices[2][0] - vertices[0][0], 
    vertices[2][1] - vertices[0][1],
  };

  double v1[2] = 
  {
    vertices[1][0] - vertices[0][0], 
    vertices[1][1] - vertices[0][1],
  };

  // Compute dot products
  double dot00 = dot(v0, v0);
  double dot01 = dot(v0, v1);
  double dot11 = dot(v1, v1);

  // Render each pixel to the buffer that is within the triangle
  double dot02, dot12, invDenom, u, v;
  for(int i = 0; i < height; i++)
  {
    for(int j = 0; j < width; j++)
    {
      // Compute vector for the current point (offset by 0.5 to be at the
      // center of the pixel)
      double v2[2] = 
      {
        (j + 0.5) - vertices[0][0],
        (i + 0.5) - vertices[0][1], 
      };

      // Compute dot products for the current point
      dot02 = dot(v0, v2);
      dot12 = dot(v1, v2);

      // Compute barycentric coordinates
      invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
      u = (dot11 * dot02 - dot01 * dot12) * invDenom;
      v = (dot00 * dot12 - dot01 * dot02) * invDenom;

      // Draw the pixel if the point is in the triangle
      if(u >= 0 && v >= 0 && u + v < 1)
      {
        buffer[i * width + j] = color;
      }
    }
  }
}


mesh_t* mesh_init(int v_count, int tri_count, double vertices[], int indices[], uint8_t colors[])
{
  mesh_t* mesh = malloc(sizeof(mesh_t));
  mesh->v_count = v_count;
  mesh->tri_count = tri_count;
  mesh->vertices = malloc(sizeof(double) * v_count * 2);
  mesh->indices = malloc(sizeof(int) * tri_count * 3);
  mesh->colors = malloc(sizeof(uint8_t) * tri_count);

  for(int i = 0; i < v_count; i++)
  {
    int xi = 2 * i;
    int yi = 2 * i + 1;
    mesh->vertices[xi] = vertices[xi];
    mesh->vertices[yi] = vertices[yi];
  }

  for(int i = 0; i < tri_count; i++)
  {
    for(int j = 0; j < 3; j++)
    {
      mesh->indices[i * 3 + j] = indices[i * 3 + j];
    }
  }

  for(int i = 0; i < tri_count; i++)
  {
    mesh->colors[i] = colors[i];
  }

  return mesh;
}


void mesh_close(mesh_t* mesh)
{
  free(mesh->vertices);
  free(mesh->indices);
  free(mesh->colors);
  free(mesh);
}


void mesh_render(uint8_t* buffer, int width, int height, mesh_t* mesh)
{
  double triangle[3][2];
  for(int t = 0; t < mesh->tri_count; t++)
  {
    // Copy the correct vertices over to the triangle array
    for(int v = 0; v < 3; v++)
    {
      int index = mesh->indices[3 * t + v];
      triangle[v][0] = mesh->vertices[2 * index];
      triangle[v][1] = mesh->vertices[2 * index + 1];
    }

    // Render the triangle
    render_triangle(buffer, width, height, triangle, mesh->colors[t]);
  }
}


void mesh_rotate(mesh_t* mesh, double angle)
{
  // Calculate the newly rotated mesh
  double angle_cos = cos(angle);
  double angle_sin = sin(angle);
  double temp[mesh->v_count * 2];
  for(int i = 0; i < mesh->v_count; i++)
  {
    int xi = 2 * i;
    int yi = 2 * i + 1;
    temp[xi] = mesh->vertices[xi] * angle_cos + mesh->vertices[yi] * angle_sin;
    temp[yi] = mesh->vertices[xi] * -angle_sin + mesh->vertices[yi] * angle_cos;
  }

  // Copy the output back to the mesh
  for(int i = 0; i < mesh->v_count; i++)
  {
    int xi = 2 * i;
    int yi = 2 * i + 1;
    mesh->vertices[xi] = temp[xi];
    mesh->vertices[yi] = temp[yi];
  }
}


void mesh_offset(mesh_t* mesh, int x, int y)
{
  for(int i = 0; i < mesh->v_count; i++)
  {
    int xi = 2 * i;
    int yi = 2 * i + 1;
    mesh->vertices[xi] += x;
    mesh->vertices[yi] += y;
  }
}


mesh_t* mesh_copy(mesh_t* mesh)
{
  return mesh_init(mesh->v_count, mesh->tri_count, mesh->vertices, mesh->indices, mesh->colors);
}
