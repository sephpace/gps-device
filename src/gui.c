/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "gui.h"


mesh_t* arrow_mesh;


int gui_init(void)
{
  // Set up FPIOA configurations
  if(fpioa_set_function(29, FUNC_UART1_TX) != 0)
  {
    return 1;
  }

  // Set up UART
  uart_init(UART_DEVICE_1);
  uart_configure(
      UART_DEVICE_1, 
      9600, 
      UART_BITWIDTH_8BIT, 
      UART_STOP_1, 
      UART_PARITY_NONE
  );

  // Set up initial screen settings
  uart_send_data(UART_DEVICE_1, "CL", 2);
  uart_send_data(UART_DEVICE_1, "SD\3", 3);
  uart_send_data(UART_DEVICE_1, "SF\6", 3);

  // Set up the GUI meshes
  double vertices[] = 
  {
    -20, 20,
    0, -20,
    20, 20,
    0, 10,
  };

  int indices[] = 
  {
    0, 1, 3,
    2, 1, 3,
  };

  uint8_t colors[2] = {255, 255};

  arrow_mesh =  mesh_init(4, 2, vertices, indices, colors);

  return 0;
}


void gui_render_arrow(uint8_t x, uint8_t y, double direction)
{
  // Initialize the buffer
  int width = 50;
  int height = 50;
  uint8_t buffer[width * height];
  for(int i = 0; i < width * height; i++)
  {
    buffer[i] = 0;
  }

  // Transform the mesh and render it to the buffer
  mesh_t* mesh = mesh_copy(arrow_mesh);
  mesh_rotate(mesh, direction);
  mesh_offset(mesh, width / 2, height / 2);
  mesh_render(buffer, width, height, mesh);

  // Draw mesh lines
  /*char command[7];*/
  /*uint8_t x1, y1, x2, y2, x3, y3;*/
  /*for(int t = 0; t < mesh->tri_count; t++)*/
  /*{*/
    /*int v1_index = mesh->indices[3 * t];*/
    /*int v2_index = mesh->indices[3 * t + 1];*/
    /*int v3_index = mesh->indices[3 * t + 2];*/
    /*x1 = (uint8_t) round(mesh->vertices[2 * v1_index]);*/
    /*y1 = (uint8_t) round(mesh->vertices[2 * v1_index + 1]);*/
    /*x2 = (uint8_t) round(mesh->vertices[2 * v2_index]);*/
    /*y2 = (uint8_t) round(mesh->vertices[2 * v2_index + 1]);*/
    /*x3 = (uint8_t) round(mesh->vertices[2 * v3_index]);*/
    /*y3 = (uint8_t) round(mesh->vertices[2 * v3_index + 1]);*/
    /*sprintf(command, "LN%c%c%c%c", x1, y1, x2, y2);*/
    /*uart_send_data(UART_DEVICE_1, command, 6);*/
    /*sprintf(command, "LN%c%c%c%c", x2, y2, x3, y3);*/
    /*uart_send_data(UART_DEVICE_1, command, 6);*/
    /*sprintf(command, "LN%c%c%c%c", x3, y3, x1, y1);*/
    /*uart_send_data(UART_DEVICE_1, command, 6);*/
  /*}*/

  mesh_close(mesh);

  // Display the buffer on the GUI
  int command_size = width * height + 10;
  char command[command_size];
  sprintf(command, "EDIM1%c%c%c%c", x, y, width, height);
  for(int i = 0; i < width * height; i++)
  {
    command[i + 9] = buffer[i];
  }
  uart_send_data(UART_DEVICE_1, command, command_size);
}


void gui_update_gps_data(gps_data_t* data)
{
  if(data->valid)
  {
    char text[25];

    // Latitude
    sprintf(text, "TTLat: %dd %d' %.2f\" %c",
        data->lat_deg, data->lat_min, data->lat_sec, data->lat_dir
    );
    uart_send_data(UART_DEVICE_1, "TP\0\0", 4);
    uart_send_data(UART_DEVICE_1, text, strlen(text)+1); 

    // Longitude
    sprintf(text, "TTLon: %dd %d' %.2f\" %c",
        data->long_deg, data->long_min, data->long_sec, data->long_dir
    );
    uart_send_data(UART_DEVICE_1, "TP\0\1", 4);
    uart_send_data(UART_DEVICE_1, text, strlen(text)+1); 

    // Altitude
    sprintf(text, "TTAlt: %.2f %c",
        data->alt, data->alt_units
    );
    uart_send_data(UART_DEVICE_1, "TP\0\2", 4);
    uart_send_data(UART_DEVICE_1, text, strlen(text)+1); 
  }
}


void gui_close()
{
  mesh_close(arrow_mesh);
}
