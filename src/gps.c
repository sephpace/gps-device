/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "gps.h"


int gps_init(void)
{
  // Set up FPIOA configurations
  if(fpioa_set_function(25, FUNC_UART2_RX) != 0)
  {
    return 1;
  }

  // Set up UART
  uart_init(UART_DEVICE_2);
  uart_configure(
      UART_DEVICE_2, 
      9600, 
      UART_BITWIDTH_8BIT, 
      UART_STOP_1, 
      UART_PARITY_NONE
  );

  return 0;
}


void gps_read_line(char* dest)
{
  int total_bytes_read = 0;
  char buffer = ' ';
  int bytes_read;

  while(buffer != '\n')
  {
    bytes_read = uart_receive_data(UART_DEVICE_2, &buffer, 1);
    if(bytes_read > 0)
    {
      dest[total_bytes_read++] = buffer;
    }
  }

  dest[total_bytes_read] = '\0';
}


void gps_data_split(char* data_line, char tokens[][12])
{
  int token_row = 0;
  int token_col = 0;
  for(int i = 1; data_line[i] != '\r'; i++)
  {
    if(data_line[i] == ',')
    {
      tokens[token_row][token_col] = '\0';
      token_row++;
      token_col = 0;
      continue;
    }

    tokens[token_row][token_col] = data_line[i];
    token_col++;
  }

  tokens[token_row][token_col] = '\0';
}


void gps_get_data(gps_data_t* data_buf)
{
  // Get GPGGA data
  char data_line[500] = "";
  while(strncmp(data_line, "$GPGGA", 6) != 0)
  {
    gps_read_line(data_line);
  }

  // Extract the data
  char tokens[15][12];
  gps_data_split(data_line, tokens);

  // Latitude
  if(strcmp(tokens[2], "") == 0)
  {
    data_buf->lat_deg = 0;
    data_buf->lat_min = 0;
    data_buf->lat_sec = 0.0f;
  }
  else
  {
    char lat_deg_str[2];
    data_buf->lat_deg = atoi(strncpy(lat_deg_str, tokens[2], 2));

    char lat_min_str[2];
    data_buf->lat_min = atoi(strncpy(lat_min_str, tokens[2]+2, 2));

    char lat_sec_str[6];
    data_buf->lat_sec = atof(strncpy(lat_sec_str, tokens[2]+4, 6)) * 60;
  }

  if(strcmp(tokens[3], "") == 0)
  {
    data_buf->lat_dir = '\0';
  }
  else
  {
    data_buf->lat_dir = tokens[3][0];
  }

  // Longitude
  if(strcmp(tokens[4], "") == 0)
  {
    data_buf->long_deg = 0;
    data_buf->long_min = 0;
    data_buf->long_sec = 0.0f;
  }
  else
  {
    char long_deg_str[3];
    data_buf->long_deg = atoi(strncpy(long_deg_str, tokens[4], 3));

    char long_min_str[2];
    data_buf->long_min = atoi(strncpy(long_min_str, tokens[4]+3, 2));

    char long_sec_str[6];
    data_buf->long_sec = atof(strncpy(long_sec_str, tokens[4]+5, 6)) * 60;
  }

  if(strcmp(tokens[5], "") == 0)
  {
    data_buf->long_dir = '\0';
  }
  else
  {
    data_buf->long_dir = tokens[5][0];
  }

  // Signal strength
  if(strcmp(tokens[8], "") == 0)
  {
    data_buf->signal = 0.0f;
  }
  else
  {
    float hdop = atof(tokens[8]);
    data_buf->signal = 1 / hdop;
  }

  // Altitude
  if(strcmp(tokens[9], "") == 0)
  {
    data_buf->alt = 0.0f;
  }
  else
  {
    data_buf->alt = atof(tokens[9]);
  }

  if(strcmp(tokens[9], "") == 0)
  {
    data_buf->alt = '\0';
  }
  else
  {
    data_buf->alt_units = tokens[10][0];
  }

  // Check if data is valid using checksum
  uint8_t sum = 0;
  for(int i = 1; data_line[i] != '*'; i++)
  {
    sum ^= data_line[i];
  }

  uint8_t checksum = strtoul(tokens[14]+1, NULL, 16);

  data_buf->valid = (sum == checksum);
}
