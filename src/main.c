/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include <math.h> // TODO: Delete me
#include <stdio.h>

#include "gps.h"
#include "gui.h"

int main(void)
{
  // Set up GUI
  if(gui_init() != 0)
  {
    printf("Failed to initialize GUI!\n");
    while(1);
  }

  // Set up GPS
  if(gps_init() != 0)
  {
    printf("Failed to initialize GPS!\n");
    while(1);
  }

  // Draw the GUI
  gps_data_t* data = malloc(sizeof(gps_data_t));
  while(1)
  {
    gps_get_data(data);
    gui_update_gps_data(data);
    gui_render_arrow(40, 60, 0.0);
  }

  gui_close();
  free(data);

  return 0;
}
