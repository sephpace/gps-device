/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef GPS_H
#define GPS_H


#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include <fpioa.h>
#include <uart.h>


/* Contains longitude, latitude, and altitude data. */
typedef struct
{
  float lat_sec;    // The latitude seconds
  float long_sec;   // The longitude seconds
  float alt;        // The altitude
  float signal;     // The strength of the signal (1 / HDOP)
  uint8_t lat_min;  // The latitude minutes
  uint8_t lat_deg;  // The latitude degrees
  uint8_t long_min; // The longitude minutes
  uint8_t long_deg; // The longitude degrees
  uint8_t valid;    // 1 if the data is valid and 0 otherwise
  char lat_dir;     // The latitude direction (N or S)
  char long_dir;    // The longitude direction (E or W)
  char alt_units;   // The units of the altitude (M or F)

} gps_data_t;


/* Initializes the GPS module.
 *
 * Returns:
 *    0             - Success
 *    Anything else - Failure
 */
int gps_init(void);


/* Reads a line of data from the GPS module.
 *
 * Parameters:
 *    dest - Destination output buffer to store the line of data.
 */
void gps_read_line(char* dest);


/* Splits the given GPS data line into tokens, separated by commas.
 *
 * Parameters:
 *    data_line - The line of GPS data. Input.
 *    tokens    - The array of tokens. Output.
 */
void gps_data_split(char* data_line, char tokens[][12]);


/* Gets data from the GPS module and uses it to fill in the given data buffer.
 *
 * Parameters:
 *    data_buf - The buffer to fill with the GPS data.
 */
void gps_get_data(gps_data_t* data_buf);


#endif // GPS_H
