/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef GRAPHICS_H
#define GRAPHICS_H


#include <math.h>
#include <stdint.h>
#include <stdlib.h>


/* Contains all information needed to render a mesh. */
typedef struct
{
  int v_count;      // The amount of vertices.
  int tri_count;    // The amount of triangles in the mesh.
  double* vertices; // The vertices of the mesh.
  int* indices;     // The indices of the mesh. Every 3 indices represents one 
                    // triangle.
  uint8_t* colors;  // The color of each triangle in 8-bit format (RRRGGGBB).
} mesh_t;


/* Performs a vector dot product on two given vectors.
 *
 * Parameters:
 *    v1 - The first vector.
 *    v2 - The second vector.
 * 
 * Returns:
 *    The result of the dot product.
 */
double dot(double v1[2], double v2[2]);


/* Renders a triangle to the given pixel buffer.
 *
 * Parameters:
 *    buffer   - The pixel buffer.
 *    width    - The width of the buffer.
 *    height   - The width of the buffer.
 *    vertices - The vertices of the triangle.
 *    color    - The 8-bit color value of the triangle.
 */
void render_triangle(uint8_t* buffer, int width, int height, double vertices[3][2], uint8_t color);


/* Initialize a mesh with the given values.
 *
 * Parameters:
 *    v_count   - The amount of vertices.
 *    tri_count - The amount of triangles in the mesh.
 *    vertices  - The vertices of the mesh. Every 2 values is one vertex.
 *    indices   - The indices of the mesh. Every 3 indices represents one triangle.
 *    colors    - The color of each triangle in 8-bit format (RRRGGGBB).
 *
 * Returns:
 *    mesh - The initialized mesh. Must be freed when no longer in use by
 *           passing it to mesh_close(mesh).
 */
mesh_t* mesh_init(int v_count, int tri_count, double vertices[], int indices[], uint8_t colors[]);


/* Frees all memory associated with the given mesh.
 * 
 * Parameters:
 *    mesh - The mesh to close.
 */
void mesh_close(mesh_t* mesh);


/* Renders a mesh to the given pixel buffer.
 *
 * Parameters:
 *    buffer    - The pixel buffer.
 *    width     - The width of the buffer.
 *    height    - The width of the buffer.
 *    mesh      - The mesh to render.
 */
void mesh_render(uint8_t* buffer, int width, int height, mesh_t* mesh);


/* Rotates the given 2D source mesh by the given angle.
 *
 * Parameters:
 *    mesh    - The mesh to rotate.
 *    angle   - The angle in radians. Input.
 */
void mesh_rotate(mesh_t* mesh, double angle);


/* Offsets the x and y values of the given 2D mesh.
 *
 * Parameters:
 *    mesh    - The mesh to offset.
 *    x       - The horizontal offset.
 *    y       - The vertical offset.
 */
void mesh_offset(mesh_t* mesh, int x, int y);


/* Returns a copy of the given mesh.
 *
 * Parameters:
 *    mesh - The mesh to copy.
 * 
 * Returns:
 *    The copied mesh.
 */
mesh_t* mesh_copy(mesh_t* mesh);


#endif // GRAPHICS_H
