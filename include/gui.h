/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef GUI_H
#define GUI_H


#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <fpioa.h>
#include <uart.h>

#include "gps.h"
#include "graphics.h"


/* Initializes the display and GUI variables.
 *
 * Returns:
 *    0             - Success
 *    Anything else - Failure
 */
int gui_init(void);


/* Renders an arrow to the GUI that points in the given direction.
 *
 * Parameters:
 *    x         - The horizontal position of the center of the arrow in pixels.
 *    y         - The vertical position of the center of the arrow in pixels.
 *    direction - The direction the arrow is facing (i.e. its rotation).
 */
void gui_render_arrow(uint8_t x, uint8_t y, double direction);


/* Updates the GPS data by displaying it on the GUI.
 *
 * Parameters:
 *    data - The data from the GPS module.
 */
void gui_update_gps_data(gps_data_t* data);


/* Cleans up the GUI after it is finished running. */
void gui_close();


#endif // GUI_H
